#!/bin/bash

DAY=$(date +'%A')
DATE=$(date +'%F')
DATE_OF_MONTH=$(date +'%d')
WEEK=$(date +'%U')
MONTH=$(date +'%B')
HOUR=$(date +'%H')
TIMESTAMP=$(date +%s)

BASE_DIR="/opt/minecraft"
TO_BACKUP="admins.txt ops.json server.properties world/ world_nether/ world_the_end/"

CURRENT_BACKUP="/tmp/Minecraft.${TIMESTAMP}.tar.gz"

BACKUP_BASE_PATH="/opt/minecraft-backup"
HOURLY_FILENAME="Minecraft.${DATE}.${HOUR}00.tar.gz"
DAILY_FILENAME="Minecraft.${DATE}.${DAY}.tar.gz"
WEEKLY_FILENAME="Minecraft.${DATE}.Week-${WEEK}.tar.gz"
MONTHLY_FILENAME="Minecraft.${DATE}.${MONTH}.tar.gz"

# Values are in days
HOURLY_KEEP=1
DAILY_KEEP=7
WEEKLY_KEEP=30
MONTHLY_KEEP=365

create_backup_folders () {
  
  mkdir -p "${BACKUP_BASE_PATH}/hourly"
  mkdir -p "${BACKUP_BASE_PATH}/daily"
  mkdir -p "${BACKUP_BASE_PATH}/weekly"
  mkdir -p "${BACKUP_BASE_PATH}/monthly"
}

create_backup() {
  echo "Creating backup..."

  # Send message to console that backup is starting
  docker exec minecraft rcon-cli say “Backup initialising…”

  # Force a save
  docker exec minecraft rcon-cli save-all

  # Disable auto save
  docker exec minecraft rcon-cli save-off

  # Run the backup
  cd ${BASE_DIR}
  tar -zcvf ${CURRENT_BACKUP} ${TO_BACKUP}

  # Enable saves
  docker exec minecraft rcon-cli save-on

  # Send message
  docker exec minecraft rcon-cli say “Backup complete.”
}

create_hourly_backup() {
  echo "Performing hourly backup..."
  
  cp ${CURRENT_BACKUP} ${BACKUP_BASE_PATH}/hourly/${HOURLY_FILENAME}
}

create_daily_backup() {
  if [ ${HOUR} != "00" ]
  then
    return
  fi
  
  echo "Performing daily backup..."
  
  cp ${CURRENT_BACKUP} ${BACKUP_BASE_PATH}/daily/${DAILY_FILENAME}
}

create_weekly_backup() {
  if ! [[ ${DAY} = "Monday" &&  ${HOUR} = "00" ]];
  then
    return
  fi
  
  echo "Performing weekly backup..."
  
  cp ${CURRENT_BACKUP} ${BACKUP_BASE_PATH}/weekly/${WEEKLY_FILENAME}
}

create_montly_backup() {
  if ! [[ ${DATE_OF_MONTH} = "01" &&  ${HOUR} = "00" ]];
  then
    return
  fi
  
  echo "Performing monthly backup..."
  
  cp ${CURRENT_BACKUP} ${BACKUP_BASE_PATH}/monthly/${MONTHLY_FILENAME}
}

cleanup_backups() {
  find ${BACKUP_BASE_PATH}/hourly -type f -mtime +${HOURLY_KEEP} -exec rm -f {} \;
  find ${BACKUP_BASE_PATH}/daily -type f -mtime +${DAILY_KEEP} -exec rm -f {} \;
  find ${BACKUP_BASE_PATH}/weekly -type f -mtime +${WEEKLY_KEEP} -exec rm -f {} \;
  find ${BACKUP_BASE_PATH}/weekly -type f -mtime +${WEEKLY_KEEP} -exec rm -f {} \;
  
  rm ${CURRENT_BACKUP}
}

restart_server() {
  if [ ${HOUR} != "06" ]
  then
    return
  fi
  
  echo "The server will automatically restart in 5 minutes..."
  
  # Send a message to console that restart is starting in 5 minutes
  docker exec minecraft rcon-cli say "The server will automatically restart in 5 minutes"
  
  sleep 4m

  echo "The server will automatically restart in 1 minute..."
  
  # Wait 4 minutes, send another message to console that restart is starting in 1 minute
  docker exec minecraft rcon-cli say "The server will automatically restart in 1 minute"
  
  sleep 1m
  
  echo "The server is restarting..."

  # Send a message to console that restart is in process
  docker exec minecraft rcon-cli say “The server is restarting“

  # Save the world before restart
  docker exec minecraft rcon-cli save-all

  # Stop MC server gracefully
  docker exec minecraft rcon-cli stop

  # Restart Docker container
  docker restart minecraft
}

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Create backup folders
create_backup_folders

# Create the initial backup
create_backup

# Perform a hourly backup
create_hourly_backup

# Perform a daily backup
create_daily_backup

# If today is Monday, create a weekly backup
#create_weekly_backup

# If today is the first of the month, create a monthly backup
#create_montly_backup

# Rotate backups
echo Clean up backups...
cleanup_backups

# Restart the Minecraft server if it is 6am
restart_server


